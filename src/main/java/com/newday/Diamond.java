package com.newday;

import java.util.Scanner;

public class Diamond {
	private final String SPACE = " ";
	
	public static void main(String[] args) {
		System.out.print("Enter a letter between A to Z : ");

	    Scanner reader = new Scanner(System.in);
	    
	    String letter = reader.next("[A-Z]");
		  Diamond diamond = new Diamond();
		  System.out.println( diamond.print(letter));
		 ;
	}
	
	String[] alphabet = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
	        "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
	        "W", "X", "Y", "Z" };
	    int letterNumber = 0;
	    
	
	public int getLetterPosition(String letterEntered) {
		 // search for letter number in the array letter
	      for (int i = 0; i < alphabet.length; i++) {
	        if (alphabet[i].equals(letterEntered)) {
	        	letterNumber = i;
	          break;
	        }
	      }
		
		return letterNumber;
	}
	
	public String print(String letter) {
		int letterIndex = getLetterPosition(letter);
		String diamondString = "";
		
		for(int letterCount = 0; letterCount <= letterIndex; letterCount++) {
			diamondString = addSpace(diamondString, letterIndex, letterCount);
			
				if(!alphabet[letterCount].equals("A")) {
					diamondString = addLetters(diamondString, letterCount);
				} else {
					diamondString += alphabet[letterCount];
				}
				diamondString += "\n";
		}

		return reverse(new StringBuilder(diamondString).reverse()
				.toString().split("\n"), diamondString);
	}
	
	private String reverse(String[] diamondStringArray, String diamondString) {
		String prefixSpace = "";
		for(int i = 1; i <= diamondStringArray.length -1; i++) {
			if(i > 1) {
				prefixSpace += SPACE;
			}
		diamondString += new StringBuilder(diamondStringArray[i])
					 .insert(0, prefixSpace).append("\n").toString();
		}
		
		return diamondString;
	}
	
	private String addLetters(String diamondString, int letterCount) {
		diamondString += alphabet[letterCount];
		diamondString = addSpace(diamondString, letterCount);
		diamondString += alphabet[letterCount];
		return diamondString;
	}
	
	private String addSpace(String diamondString, int letterIndex, int letterCount) {
		for (int spaceCount = 0; spaceCount < letterIndex - letterCount; spaceCount++) {
			diamondString += SPACE;
		}
		return diamondString;
	}
	
	private String addSpace(String diamondString, int index) {
		for (int j = 1; j <= 2 * index - 1; j++) {
			 diamondString  += SPACE;
	        }
		return diamondString ;
	}

}

	
