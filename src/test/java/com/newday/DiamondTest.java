package com.newday;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DiamondTest {
	
	Diamond diamond;

	@Before
	public void setUp() throws Exception {
		diamond = new Diamond();
	}

	@Test
	public void testGetLetterPosition() {
		assertEquals(Integer.valueOf(1), Integer.valueOf(diamond.getLetterPosition("B")));
	}
	
	@Test
	public void testCharacterSequence() {
		assertEquals("  A\n B B\nC   C\nC   C\n B B \n  A  \n", diamond.print("C"));
	}

}
